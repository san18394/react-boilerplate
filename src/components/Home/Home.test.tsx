import React from 'react';
import { shallow } from 'enzyme';
import { HomeBaseComponent } from './Home';

const mockIncrementfn = jest.fn();
const mockLoginfn = jest.fn();
const mockLogoutfn = jest.fn();
describe('Testing the connected component without mock store, when the primary button is clicked', () => {
    let wrapper;
    beforeEach(() => {
        // pass the mock function as the home prop
        wrapper = shallow(
            <HomeBaseComponent incrementCounter={mockIncrementfn} logOut={mockLogoutfn} logIn={mockLoginfn} />
        );
    });
    it('should call the mock increment function', () => {
        wrapper.find('#increment').simulate('click', { preventDefault() {} });
        expect(mockIncrementfn.mock.calls.length).toBe(1);
        mockIncrementfn.mockRestore();
    });

    it('should call the mock login function', () => {
        wrapper.find('#logIn').simulate('click', { preventDefault() {} });
        expect(mockLoginfn.mock.calls.length).toBe(1);
        mockLoginfn.mockRestore();
    });

    it('should call the mock logout function', () => {
        wrapper.find('#logOut').simulate('click', { preventDefault() {} });
        expect(mockLogoutfn.mock.calls.length).toBe(1);
        mockLogoutfn.mockRestore();
    });
});
