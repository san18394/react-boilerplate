import React from 'react';
import './Home.scss';
import { Button } from 'antd';
import { connect } from 'react-redux';
import IHomeProps from '../../interfaces/IHome';
import { incrementCounterDispatcher } from '../../store/dispatchers/Home/DHome';
import { authenticateDispatcher, logOutDispatcher } from '../../store/dispatchers/Authentication/DAuthentication';

const HomeBaseComponent = (props: IHomeProps) => {
    const { logIn, logOut, incrementCounter } = props;
    return (
        <div className="Home">
            <header className="Home-header">
                <h3>UI toolkit from ant-design</h3>
                <span className="textMixin">Hello truncated text with mixin scss</span>
                <div className="Home-link">React with scss variable</div>
                <Button type="dashed" id="increment" onClick={incrementCounter}>
                    Increment
                </Button>
                <Button type="primary" id="logIn" onClick={logIn}>
                    Login
                </Button>
                <Button type="danger" id="logOut" onClick={logOut}>
                    Logout
                </Button>
            </header>
        </div>
    );
};
const dispatchers = {
    incrementCounter: incrementCounterDispatcher,
    logOut: logOutDispatcher,
    logIn: authenticateDispatcher
};

const Home = connect(
    null,
    dispatchers
)(HomeBaseComponent);

export { Home, HomeBaseComponent };
