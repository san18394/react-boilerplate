import React from 'react';
import './About.scss';
import { Button } from 'antd';
import { connect } from 'react-redux';
import { IAbout } from '../../interfaces/IAbout';

const About = (props: IAbout) => {
    const { ctr, incrementCounter } = props;
    return (
        <div className="About">
            About Page
            <br />
            Value from redux statemanagement <strong> {ctr} </strong>.
            <br />
            <Button type="primary" onClick={incrementCounter}>
                Primary
            </Button>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        ctr: state.counter
    };
};

const mapDispatchToProps = dispatch => {
    return {
        incrementCounter: () => dispatch({ type: 'INCREMENT' })
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(About);
