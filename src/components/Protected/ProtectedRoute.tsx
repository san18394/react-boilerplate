import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { getAuthenticatedState } from '../../store/selectors/Protected/SProtected';

const ProtectedRoute = ({ authenticated, component: Component, ...rest }: any) => {
    return (
        <Route
          {...rest}
          render={props => {
                if (authenticated) {
                    return <Component {...props} />;
                }
                return (
                    <Redirect
                      to={{
                            pathname: '/',
                            state: {
                                from: props.location
                            }
                        }}
                    />
                );
            }}
        />
    );
};

export default connect(state => ({ authenticated: getAuthenticatedState(state) }))(ProtectedRoute);
