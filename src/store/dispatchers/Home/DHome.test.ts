import { incrementCounterDispatcher } from './DHome';

describe('Increment actions', () => {
    it('should dispatch INCREMENT action', () => {
        expect(incrementCounterDispatcher()).toEqual({ type: 'INCREMENT'});
    })
});