export const authenticateDispatcher = () => {
    return { type: 'AUTHENTICATE' };
};

export const logOutDispatcher = () => {
    return { type: 'LOGOUT' };
};