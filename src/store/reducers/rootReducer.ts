import { IReducerState, IActionState } from "../../interfaces/IReducer";

const initialState: IReducerState = {
  counter: 11,
  authenticated: true
};

const rootReducer = (state = initialState, action: IActionState) => {
  switch (action.type) {
    case "INCREMENT":
      return {
        ...state,
        counter: state.counter + 1
      };
    case "AUTHENTICATE":
      return { ...state, authenticated: true };
    case "LOGOUT":
      return { ...state, authenticated: false };
    default:
      return state;
  }
};

export default rootReducer;
