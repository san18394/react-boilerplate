import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import { Menu, Icon } from 'antd';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './store/reducers/rootReducer';
import { Home } from './components/Home/Home';
import About from './components/About/About';
import Error from './components/Error/Error';
import ProtectedRoute from './components/Protected/ProtectedRoute';

const RStore = createStore(rootReducer);

const routing = (
    <Router>
        <Menu mode="horizontal" defaultSelectedKeys={['mail']}>
            <Menu.Item key="mail">
                <Link to="/">
                    <Icon type="mail" />
                    Home
                </Link>
            </Menu.Item>
            <Menu.Item key="app">
                <Link to="/about">
                    <Icon type="appstore" />
                    About
                </Link>
            </Menu.Item>
        </Menu>
        <Switch>
            <Route path="/" exact component={Home} />
            <ProtectedRoute path="/about" exact component={About} />
            <Route component={Error} />
        </Switch>
    </Router>
);

ReactDOM.render(<Provider store={RStore}>{routing}</Provider>, document.getElementById('root'));
