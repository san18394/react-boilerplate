export interface IReducerState{
    counter:number,
    authenticated: boolean
}

export interface IActionState{
    type : string | number,
    payload ?: any 
}