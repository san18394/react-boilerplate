import * as React from "react";

// typescript way of creating props for functional components

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default interface HomeProps extends React.Props<any> {
  incrementCounter(): void;
  logOut(): void;
  logIn(): void;
}
