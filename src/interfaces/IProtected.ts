export interface IProtectedStates {
    authenticated : boolean,
    authenticatorFunc() : void
}